package edu.ubb.Test;

import edu.ubb.CurrencyConverterWindow;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Tests {
    CurrencyConverterWindow currencyConveretWindow = new CurrencyConverterWindow();

    @Test
    public void calculateLei() {
        currencyConveretWindow.setEuroText("10");
        currencyConveretWindow.calculateLei();
        assertEquals("46.5", currencyConveretWindow.getLeiText());
    }

    @Test
    public void calculateEuro() {
        currencyConveretWindow.setLeiText("10");
        currencyConveretWindow.calculateEuro();
        assertEquals("2.150537634408602", currencyConveretWindow.getEuroText());
    }

}

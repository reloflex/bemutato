package edu.ubb;

import javax.swing.*;
import java.awt.*;

public class Main {

    public static void main(String[] args) {
        CurrencyConverterWindow frame = new CurrencyConverterWindow();
        frame.setTitle("Currency Converter");
        frame.setVisible(true);
        frame.setBounds(100, 100, 300, 300);
    }
}

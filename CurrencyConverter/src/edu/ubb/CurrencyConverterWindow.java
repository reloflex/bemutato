package edu.ubb;

import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class CurrencyConverterWindow extends Frame {
    Label leiLabel;
    Label euroLabel;
    TextField leiTextField;
    TextField euroTextField;

    public CurrencyConverterWindow() {

        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        leiLabel = new Label("Lej: ");
        euroLabel = new Label("Euro: ");
        leiTextField = new TextField();
        euroTextField = new TextField();
        leiTextField.setText("0.0");
        euroTextField.setText("0.0");

        leiTextField.setBounds(128, 100, 86, 20);
        leiTextField.setColumns(10);
        euroTextField.setBounds(128, 200, 86, 20);
        euroTextField.setColumns(100);

        leiLabel.setBounds(65, 100, 46, 14);
        euroLabel.setBounds(65, 200, 46, 14);

        leiTextField.addFocusListener(new SymFocus());
        euroTextField.addFocusListener(new SymFocus());

        this.setLayout(null);
        this.add(leiLabel);
        this.add(leiTextField);
        this.add(euroLabel);
        this.add(euroTextField);

    }

    private class SymFocus extends FocusAdapter {
        public void focusLost(FocusEvent event) {
            Object object = event.getSource();
            if (object == leiTextField)
                LeiTextField_FocusLost(event);
            else if (object == euroTextField)
            EuroTextField_FocusLost(event);

        }

        public void focusGained(FocusEvent event) {

        }
    }

    public void LeiTextField_FocusLost(FocusEvent event) {
        if (!leiTextField.getText().matches("-?\\d+(\\.\\d+)?"))
            leiTextField.setText("0");
        calculateEuro();
    }

    public void EuroTextField_FocusLost(FocusEvent event) {
        if (!euroTextField.getText().matches("-?\\d+(\\.\\d+)?"))
            euroTextField.setText("0");
        calculateLei();
    }

    public void calculateLei() {
        try {
            System.out.println(euroTextField.getText());
            float euro = Float.parseFloat(euroTextField.getText());
            leiTextField.setText(String.valueOf(euro * 4.65));
        } catch (NumberFormatException e) {
            throw new RuntimeException("Unexpected Number Format Error");
        }
    }

    public void calculateEuro() {
        try {
            float lei = Float.parseFloat(leiTextField.getText());
            euroTextField.setText(String.valueOf(lei / 4.65));
        } catch (NumberFormatException e) {
            throw new RuntimeException("Unexpected Number Format Error");
        }
    }

    public String getLeiText() {
        return leiTextField.getText();
    }

    public String getEuroText() {
        return euroTextField.getText();
    }

    public void setLeiText(String leiTextField) {
        this.leiTextField.setText(leiTextField);
    }

    public void setEuroText(String euroTextField) {
        this.euroTextField.setText(euroTextField);
    }
}

package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Female;
import model.Male;
import model.Person;

class PersonTest {

	@Test
	void testMale() {
		Person female = new Female();
		assertEquals('F', female.getCode());
		assertEquals(false, female.isMale());
	}

	@Test
	void testFemale() {
		Person male = new Male();
		assertEquals('M', male.getCode());
		assertEquals(true, male.isMale());
	}
	
}

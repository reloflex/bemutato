package model;

public class Male extends Person {

	@Override
	public boolean isMale() {
		return true;
	}

	@Override
	public char getCode() {
		return 'M';
	}
}

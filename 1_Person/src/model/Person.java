package model;

public abstract class Person {
	public abstract boolean isMale();
	public abstract char getCode();
}

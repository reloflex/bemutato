package model;

public class Female extends Person {

	@Override
	public boolean isMale() {
		return false;
	}

	@Override
	public char getCode() {
		return 'F';
	}
}

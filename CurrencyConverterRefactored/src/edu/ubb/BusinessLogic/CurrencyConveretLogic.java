package edu.ubb.BusinessLogic;

import java.util.Observable;

public class CurrencyConveretLogic extends Observable {
    private float euro = 0;
    private float lei = 0;

    public float getEuro() {
        return euro;
    }

    public float getLei() {
        return lei;
    }

    public void setEuro(float euro) {
        this.euro = euro;
        setChanged();
        notifyObservers();
    }

    public void setLei(float lei) {
        this.lei = lei;
        setChanged();
        notifyObservers();
    }

    public void calculateLei() {
        try {
            setLei(Float.parseFloat(String.valueOf( getEuro() * 4.65)));
        } catch (NumberFormatException e) {
            throw new RuntimeException("Unexpected Number Format Error");
        }
    }

    public void calculateEuro() {
        try {
            setEuro(Float.parseFloat(String.valueOf( getLei() / 4.65)));
        } catch (NumberFormatException e) {
            throw new RuntimeException("Unexpected Number Format Error");
        }
    }

}

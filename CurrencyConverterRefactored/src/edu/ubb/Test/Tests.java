package edu.ubb.Test;

import edu.ubb.BusinessLogic.CurrencyConveretLogic;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;


public class Tests {
    CurrencyConveretLogic currencyConveretLogic = new CurrencyConveretLogic();

    @Test
    public void calculateLei() {
        currencyConveretLogic.setEuro(10);
        currencyConveretLogic.calculateLei();
        assertEquals(46.5, currencyConveretLogic.getLei());
    }

    @Test
    public void calculateEuro() {
        currencyConveretLogic.setLei(10);
        currencyConveretLogic.calculateEuro();
        assertEquals(2.1505377292633057, currencyConveretLogic.getEuro());
    }

}

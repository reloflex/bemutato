package edu.ubb.UI;

import edu.ubb.BusinessLogic.CurrencyConveretLogic;

import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Observable;
import java.util.Observer;

public class CurrencyConverterWindow extends Frame implements Observer {
    private Label leiLabel;
    private Label euroLabel;
    private TextField leiTextField;
    private TextField euroTextField;
    private CurrencyConveretLogic currencyConveretLogic;

    private float euro;
    private float lei;

    public CurrencyConverterWindow() {

        this.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        leiLabel = new Label("Lej: ");
        euroLabel = new Label("Euro: ");
        leiTextField = new TextField();
        euroTextField = new TextField();

        leiTextField.setBounds(128, 100, 86, 20);
        leiTextField.setColumns(10);
        euroTextField.setBounds(128, 200, 86, 20);
        euroTextField.setColumns(100);

        leiLabel.setBounds(65, 100, 46, 14);
        euroLabel.setBounds(65, 200, 46, 14);

        leiTextField.addFocusListener(new SymFocus());
        euroTextField.addFocusListener(new SymFocus());

        currencyConveretLogic = new CurrencyConveretLogic();
        currencyConveretLogic.addObserver(this);
        update(currencyConveretLogic, null);

        this.setLayout(null);
        this.add(leiLabel);
        this.add(leiTextField);
        this.add(euroLabel);
        this.add(euroTextField);

    }

    @Override
    public void update(Observable o, Object arg) {
        leiTextField.setText(String.valueOf(currencyConveretLogic.getLei()));
        euroTextField.setText(String.valueOf(currencyConveretLogic.getEuro()));
    }

    private class SymFocus extends FocusAdapter {
        public void focusLost(FocusEvent event) {
            Object object = event.getSource();
            if (object == leiTextField)
                LeiTextField_FocusLost(event);
            else if (object == euroTextField)
                EuroTextField_FocusLost(event);

        }

        public void focusGained(FocusEvent event) {

        }
    }

    public void LeiTextField_FocusLost(FocusEvent event) {
        setLeiText(leiTextField.getText());
        if (!getLeiText().matches("-?\\d+(\\.\\d+)?"))
            setLeiText("0");
        currencyConveretLogic.calculateEuro();
    }

    public void EuroTextField_FocusLost(FocusEvent event) {
        setEuroText(euroTextField.getText());
        if (!getEuroText().matches("-?\\d+(\\.\\d+)?"))
            setEuroText("0");
        currencyConveretLogic.calculateLei();
    }


    public String getEuroText() {
        return String.valueOf(currencyConveretLogic.getEuro());
    }

    public String getLeiText() {
        return  String.valueOf(currencyConveretLogic.getLei());
    }

    public void setEuroText(String euro) {
        currencyConveretLogic.setEuro(Float.parseFloat(euro));
    }

    public void setLeiText(String lei) {
        currencyConveretLogic.setLei(Float.parseFloat(lei));
    }

}

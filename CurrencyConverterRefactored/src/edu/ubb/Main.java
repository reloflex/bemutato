package edu.ubb;

import edu.ubb.UI.CurrencyConverterWindow;

import javax.swing.*;

public class Main {

    public static void main(String[] args) {
        CurrencyConverterWindow frame = new CurrencyConverterWindow();
        frame.setTitle("Currency Converter");
        frame.setVisible(true);
        frame.setBounds(100, 100, 300, 300);    }
}

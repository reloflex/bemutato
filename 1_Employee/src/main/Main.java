package main;

import ui.EmployeeManagerFrame;

/**
 * Example with type code without any refactoring
 * @author Arnold
 */

public class Main {
	public static void main(String[] args) {
		EmployeeManagerFrame app = new EmployeeManagerFrame();
		app.init();
	}
}

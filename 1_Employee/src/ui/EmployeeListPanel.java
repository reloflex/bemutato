package ui;

import java.awt.*;
import java.awt.event.*;
import java.util.List;

import javax.swing.*;
import javax.swing.event.*;

import employee.Employee;

public class EmployeeListPanel extends JPanel implements ListSelectionListener {

	private static final long serialVersionUID = 8261753010617265130L;
	
	private JList<Employee> list;
    private DefaultListModel<Employee> listModel;

    private static final String nameString = "Name";
    private static final String typeString = "Employee type";
    private static final String levelString = "Level";
    private static final String salaryString = "Salary";
    private static final String hireString = "Hire";
    private static final String fireString = "Fire";
    private static final String upgradeTypeString = "Upgrade Type";
    private static final String downgradeTypeString = "Downgrade Type";
    private static final String upgradeLevelString = "Upgrade Level";
    private static final String downgradeLevelString = "Downgrade Level";
    
    private JButton fireButton = new JButton(fireString);
    private JButton hireButton = new JButton(hireString);
    private JButton upgradeTypeButton = new JButton(upgradeTypeString);
    private JButton downgradeTypeButton = new JButton(downgradeTypeString);
    private JButton upgradeLevelButton = new JButton(upgradeLevelString);
    private JButton downgradeLevelButton = new JButton(downgradeLevelString);
    private JTextField employeeName = new JTextField(10);

    private JLabel nameLabel = new JLabel(nameString);
	private JLabel typeLabel = new JLabel(typeString);
	private JLabel levelLabel = new JLabel(levelString);
	private JLabel salaryLabel = new JLabel(salaryString);

	private JTextArea nameTextArea = new JTextArea();
	private JTextArea typeTextArea = new JTextArea();
	private JTextArea levelTextArea = new JTextArea();
	private JTextArea salaryTextArea = new JTextArea();

    public EmployeeListPanel(List<Employee> employees) {
        super(new BorderLayout());

        listModel = new DefaultListModel<Employee>();
        
        if (employees != null && employees.size() > 0) {
        	for (Employee employee : employees) {
        		listModel.addElement(employee);
			}
        }

        list = new JList<Employee>(listModel);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        list.addListSelectionListener(this);
        list.setVisibleRowCount(5);
        JScrollPane listScrollPane = new JScrollPane(list);
        
        JPanel detaisPanel = new JPanel();
        
        detaisPanel.setLayout(new BoxLayout(detaisPanel, BoxLayout.Y_AXIS));
        detaisPanel.add(nameLabel);
        detaisPanel.add(nameTextArea);
        
        detaisPanel.add(typeLabel);
        detaisPanel.add(typeTextArea);
        
        detaisPanel.add(levelLabel);
        detaisPanel.add(levelTextArea);
        
        detaisPanel.add(salaryLabel);
        detaisPanel.add(salaryTextArea);

        HireListener hireListener = new HireListener(hireButton);
        hireButton.setActionCommand(hireString);
        hireButton.addActionListener(hireListener);
        hireButton.setEnabled(false);

        fireButton.setActionCommand(fireString);
        fireButton.addActionListener(new FireListener(fireButton));
        
        upgradeTypeButton.addActionListener(new UpgradeTypeListener(upgradeTypeButton));
        upgradeTypeButton.setActionCommand(upgradeTypeString);
        
        downgradeTypeButton.addActionListener(new DowngradeTypeListener(downgradeTypeButton));
        downgradeTypeButton.setActionCommand(downgradeTypeString);
        
        upgradeLevelButton.addActionListener(new UpgradeLevelListener(upgradeLevelButton));
        upgradeLevelButton.setActionCommand(upgradeLevelString);
        
        downgradeLevelButton.addActionListener(new DowngradeLevelListener(downgradeLevelButton));
        downgradeLevelButton.setActionCommand(downgradeLevelString);

        employeeName.addActionListener(hireListener);
        employeeName.getDocument().addDocumentListener(hireListener);
        
        Employee selectedEmployee = listModel.getElementAt(list.getSelectedIndex());
        updateFieldsBySelectedElement(selectedEmployee);

        JPanel buttonPane = new JPanel();
        buttonPane.setLayout(new BoxLayout(buttonPane,BoxLayout.LINE_AXIS));
        buttonPane.add(fireButton);
        buttonPane.add(upgradeTypeButton);
        buttonPane.add(downgradeTypeButton);
        buttonPane.add(upgradeLevelButton);
        buttonPane.add(downgradeLevelButton);
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(new JSeparator(SwingConstants.VERTICAL));
        buttonPane.add(Box.createHorizontalStrut(5));
        buttonPane.add(employeeName);
        buttonPane.add(hireButton);
        buttonPane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

        add(listScrollPane, BorderLayout.WEST);
        add(detaisPanel, BorderLayout.CENTER);
        add(buttonPane, BorderLayout.PAGE_END);
    }

    class FireListener implements ActionListener {
    	private JButton button;

		public FireListener(JButton button) {
			this.button = button;
		}
		
        public void actionPerformed(ActionEvent e) {
            int index = list.getSelectedIndex();
            Employee employeeToDelet = listModel.get(index);
            removeElement(index, employeeToDelet);

            int size = listModel.getSize();

            if (size == 0) {
            	button.setEnabled(false);
            } else {
                if (index == listModel.getSize()) {
                    index--;
                }

                list.setSelectedIndex(index);
                list.ensureIndexIsVisible(index);
            }
        }
    }

    class HireListener implements ActionListener, DocumentListener {
        private boolean alreadyEnabled = false;
        private JButton button;

        public HireListener(JButton button) {
            this.button = button;
        }

        public void actionPerformed(ActionEvent e) {
            String name = employeeName.getText();

            if (name.equals("") || alreadyInList(name)) {
                Toolkit.getDefaultToolkit().beep();
                employeeName.requestFocusInWindow();
                employeeName.selectAll();
                return;
            }

            int index = list.getSelectedIndex();
            if (index == -1) {
                index = 0;
            } else {
                index++;
            }

            Employee newEmployee = new Employee(Employee.ENGINEER);
            newEmployee.name = employeeName.getText();
            
            addElement(index, newEmployee);

            employeeName.requestFocusInWindow();
            employeeName.setText("");

            list.setSelectedIndex(index);
            list.ensureIndexIsVisible(index);
        }

        protected boolean alreadyInList(String name) {
        	boolean exists = false;
        	Object[] employeeList = listModel.toArray();
        	for (Object employeeObject : employeeList) {
        		if (employeeObject instanceof Employee ) {
        			Employee employee = (Employee)employeeObject;
        			
        			if (employee.name.equalsIgnoreCase(name)) {
        				exists = true;
        				break;
        			}
        		}
			}
            return exists;
        }

        public void insertUpdate(DocumentEvent e) {
            enableButton();
        }

        public void removeUpdate(DocumentEvent e) {
            handleEmptyTextField(e);
        }

        public void changedUpdate(DocumentEvent e) {
            if (!handleEmptyTextField(e)) {
                enableButton();
            }
        }

        private void enableButton() {
            if (!alreadyEnabled) {
                button.setEnabled(true);
            }
        }

        private boolean handleEmptyTextField(DocumentEvent e) {
            if (e.getDocument().getLength() <= 0) {
                button.setEnabled(false);
                alreadyEnabled = false;
                return true;
            }
            return false;
        }
    }
    
	class UpgradeTypeListener implements ActionListener {
		private JButton button;

		public UpgradeTypeListener(JButton button) {
			this.button = button;
		}
		
		public void actionPerformed(ActionEvent e) {
			int size = listModel.getSize();

			if (size == 0) {
				button.setEnabled(false);
			} else {
				Employee employee = listModel.get(list.getSelectedIndex());
				employee.upgradeEmployeeType();
				updateFieldsBySelectedElement(employee);
			}
		}
    }
    
    class DowngradeTypeListener implements ActionListener {
    	private JButton button;

		public DowngradeTypeListener(JButton button) {
			this.button = button;
		}
		
    	public void actionPerformed(ActionEvent e) {
			int size = listModel.getSize();

			if (size == 0) {
				button.setEnabled(false);
			} else {
				Employee employee = listModel.get(list.getSelectedIndex());
				employee.downgradeEmployeeType();
				updateFieldsBySelectedElement(employee);
			}
        }
    }
    
    class UpgradeLevelListener implements ActionListener {
    	private JButton button;

		public UpgradeLevelListener(JButton button) {
			this.button = button;
		}
		
		public void actionPerformed(ActionEvent e) {
			int size = listModel.getSize();

			if (size == 0) {
				button.setEnabled(false);
			} else {
				Employee employee = listModel.get(list.getSelectedIndex());
				employee.upgradeEmployeeLevel();
				updateFieldsBySelectedElement(employee);
			}
		}
    }
    
    class DowngradeLevelListener implements ActionListener {
    	private JButton button;

		public DowngradeLevelListener(JButton button) {
			this.button = button;
		}
		
    	public void actionPerformed(ActionEvent e) {
			int size = listModel.getSize();

			if (size == 0) {
				button.setEnabled(false);
			} else {
				Employee employee = listModel.get(list.getSelectedIndex());
				employee.downgradeEmployeeLevel();
				updateFieldsBySelectedElement(employee);
			}
        }
    }

    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting() == false) {

            if (list.getSelectedIndex() == -1) {
                fireButton.setEnabled(false);
            } else {
                fireButton.setEnabled(true);
        		Employee selectedEmployee = listModel.getElementAt(list.getSelectedIndex());
                updateFieldsBySelectedElement(selectedEmployee);
            }
        }
    }

	private void updateFieldsBySelectedElement(Employee employee) {
		nameTextArea.setText(employee.name);
		typeTextArea.setText(employee.getTypeName());
		levelTextArea.setText(employee.getLevelName());
		salaryTextArea.setText(employee.payAmount().toString());
	}
	
	private void addElement(int index, Employee newEmployee) {
		listModel.insertElementAt(newEmployee, index);
	}
	
	private void removeElement(int index, Employee employeeToDelet) {
		listModel.remove(index);
	}

}
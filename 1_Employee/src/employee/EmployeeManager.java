package employee;

import java.util.ArrayList;
import java.util.List;

public class EmployeeManager {
	List<Employee> employees = new ArrayList<>();
	
	public void loadEmployees() {
		// TODO: load from db
		Employee employeeEngineer = new Employee(Employee.ENGINEER);
		employeeEngineer.name = "Beke Baba";
		
		Employee employeeSalesman = new Employee(Employee.SALESMAN);
		employeeSalesman.name = "Teke Tata";
		
		Employee employeeManager = new Employee(Employee.MANAGER);
		employeeManager.name = "Meke Mama";
		
		employees.add(employeeEngineer);
		employees.add(employeeSalesman);
		employees.add(employeeManager);
	}
	
	public void saveEmployees() {
		// TODO: store in db
	}
	
	public List<Employee> getEmployees() {
		return employees;
	}
	
	public void addEmployee(Employee employee) {
		employees.add(employee);
	}
	
	public void removeEmployee(Employee employee) {
		employees.remove(employee);
	}
}

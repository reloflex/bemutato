package employee;

/**
 * Employee with type code
 * @author Arnold
 *
 */
public class Employee {
	public String name;
	public int type = ENGINEER;
	public int levelType = LEVEL_JUNIOR;
	public int monthlySalary = 100;
	public int commission = 20;
	public int bonus = 30;
	
	public static final int ENGINEER = 0;
	public static final int SALESMAN = 1;
	public static final int MANAGER = 2;
	
	public static final int LEVEL_JUNIOR = 1;
	public static final int LEVEL_MID = 2;
	public static final int LEVEL_SENIOR = 3;
	
	public Employee (int type) {
		this.type = type;
	}
	
	public Integer payAmount( ) {
		switch (type) {
			case ENGINEER:
				return monthlySalary;
			case SALESMAN:
				return monthlySalary + commission;
			case MANAGER:
				return monthlySalary + bonus;
			default:
				throw new RuntimeException("Incorrect employee");
		}
	}
	
	public void upgradeEmployeeLevel() {
		if (levelType == Employee.LEVEL_JUNIOR) {
			levelType = Employee.LEVEL_MID;
		} else if (levelType == Employee.LEVEL_MID) {
			levelType = Employee.LEVEL_SENIOR;
		}
	}
	
	public void downgradeEmployeeLevel() {
		if (levelType == Employee.LEVEL_SENIOR) {
			levelType = Employee.LEVEL_MID;
		} else if (levelType == Employee.LEVEL_MID) {
			levelType = Employee.LEVEL_JUNIOR;
		}
	}
	
	public void upgradeEmployeeType() {
		if (type == Employee.ENGINEER) {
			type = Employee.SALESMAN;
		} else if (type == Employee.SALESMAN) {
			type = Employee.MANAGER;
		}
	}
	
	public void downgradeEmployeeType() {
		if (type == Employee.MANAGER) {
			type = Employee.SALESMAN;
		} else if (type == Employee.SALESMAN) {
			type = Employee.ENGINEER;
		}
	}
	
	public String getTypeName() {
		switch (type) {
			case ENGINEER:
				return "Engineer";
			case SALESMAN:
				return "Salesman";
			case MANAGER:
				return "Manager";
			default:
				return "";
		}
	}
	
	public String getLevelName() {
		switch (levelType) {
			case LEVEL_JUNIOR:
				return "Junior";
			case LEVEL_MID:
				return "Middle";
			case LEVEL_SENIOR:
				return "Senior";
			default:
				return "";
		}
	}
	
	@Override
	public String toString() {
		return name + " - " + getTypeName();
	}
}

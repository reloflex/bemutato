package test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import employee.Employee;

class EmployeeTest {

	@Test
	void testEmployeeEngineer() {
		Employee employee = new Employee(Employee.ENGINEER);
		employee.monthlySalary = 100;
		employee.commission = 10;
		employee.bonus = 20;
		
		assertEquals(100, employee.payAmount().intValue(), "payAmount");
		
		Employee employee2 = new Employee(Employee.ENGINEER);
		employee2.monthlySalary = 100;
		employee2.commission = 10;
		employee2.bonus = 20;
		employee2.levelType = 1;
		
		assertEquals(100, employee2.payAmount().intValue(), "payAmount");
	}
	
	@Test
	void testEmployeeSalesman() {
		Employee employee = new Employee(Employee.SALESMAN);
		employee.monthlySalary = 100;
		employee.commission = 10;
		employee.bonus = 20;
		
		assertEquals(110, employee.payAmount().intValue(), "payAmount");
		
		Employee employee2 = new Employee(Employee.SALESMAN);
		employee2.monthlySalary = 100;
		employee2.commission = 10;
		employee2.bonus = 20;
		employee2.levelType = 2;
		
		assertEquals(110, employee2.payAmount().intValue(), "payAmount");
	}
	
	@Test
	void testEmployeeManager() {
		Employee employee = new Employee(Employee.MANAGER);
		employee.monthlySalary = 100;
		employee.commission = 10;
		employee.bonus = 20;
		
		assertEquals(120, employee.payAmount().intValue(), "payAmount");
		
		Employee employee2 = new Employee(Employee.MANAGER);
		employee2.monthlySalary = 100;
		employee2.commission = 10;
		employee2.bonus = 20;
		employee2.levelType = 3;
		
		assertEquals(120, employee2.payAmount().intValue(), "payAmount");
	}

}

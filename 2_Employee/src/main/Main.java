package main;

import ui.EmployeeManagerFrame;

/**
 * @author Arnold
 */
public class Main {
	public static void main(String[] args) {
		EmployeeManagerFrame app = new EmployeeManagerFrame();
		app.init();
	}
}

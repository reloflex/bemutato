package test;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import employee.Employee;
import employee.level.EmployeeLevel;
import employee.type.EmployeeType;

class EmployeeTest {

	@Test
	void testEmployeeEngineer() {
		Employee employee = new Employee(EmployeeType.ENGINEER);
		employee.setMonthlySalary(100);
		employee.setCommission(10);
		employee.setBonus(20);
		
		assertEquals(100, employee.payAmount().intValue(), "payAmount");
		
		Employee employee2 = new Employee(EmployeeType.ENGINEER);
		employee2.setMonthlySalary(100);
		employee2.setCommission(10);
		employee2.setBonus(20);
		employee2.setLevel(EmployeeLevel.JUNIOR);
		
		assertEquals(100, employee2.payAmount().intValue(), "payAmount");
	}
	
	@Test
	void testEmployeeSalesman() {
		Employee employee = new Employee(EmployeeType.SALESMAN);
		employee.setMonthlySalary(100);
		employee.setCommission(10);
		employee.setBonus(20);
		
		assertEquals(110, employee.payAmount().intValue(), "payAmount");
		
		Employee employee2 = new Employee(EmployeeType.SALESMAN);
		employee2.setMonthlySalary(100);
		employee2.setCommission(10);
		employee2.setBonus(20);
		employee2.setLevel(EmployeeLevel.MID);
		
		assertEquals(110, employee2.payAmount().intValue(), "payAmount");
	}
	
	@Test
	void testEmployeeManager() {
		Employee employee = new Employee(EmployeeType.MANAGER);
		employee.setMonthlySalary(100);
		employee.setCommission(10);
		employee.setBonus(20);
		
		assertEquals(120, employee.payAmount().intValue(), "payAmount");
		
		Employee employee2 = new Employee(EmployeeType.MANAGER);
		employee2.setMonthlySalary(100);
		employee2.setCommission(10);
		employee2.setBonus(20);
		employee2.setLevel(EmployeeLevel.SENIOR);
		
		assertEquals(120, employee2.payAmount().intValue(), "payAmount");
	}

}

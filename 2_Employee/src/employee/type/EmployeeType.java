package employee.type;

import employee.Employee;

public abstract class EmployeeType {
	public abstract int getTypeCode();
	public abstract Integer payAmount(Employee employee);

	public static final int ENGINEER = 0;
	public static final int SALESMAN = 1;
	public static final int MANAGER = 2;

	public static EmployeeType newType(int type) {
		switch (type) {
			case ENGINEER:
				return new Engineer();
			case SALESMAN:
				return new Salesman();
			case MANAGER:
				return new Manager();
			default:
				throw new RuntimeException("Incorrect employee");
		}
	}
}

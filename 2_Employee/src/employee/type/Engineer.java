package employee.type;

import employee.Employee;

public class Engineer extends EmployeeType {

	@Override
	public int getTypeCode() {
		return EmployeeType.ENGINEER;
	}
	
	public Integer payAmount(Employee employee) {
		return employee.getMonthlySalary();
	}

}

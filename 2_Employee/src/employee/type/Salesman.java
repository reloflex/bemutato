package employee.type;

import employee.Employee;

public class Salesman extends EmployeeType {

	@Override
	public int getTypeCode() {
		return EmployeeType.SALESMAN;
	}

	public Integer payAmount(Employee employee) {
		return employee.getMonthlySalary() + employee.getCommission();
	}

}
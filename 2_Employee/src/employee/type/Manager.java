package employee.type;

import employee.Employee;

public class Manager extends EmployeeType {

	@Override
	public int getTypeCode() {
		return EmployeeType.MANAGER;
	}

	public Integer payAmount(Employee employee) {
		return employee.getMonthlySalary() + employee.getBonus();
	}

}
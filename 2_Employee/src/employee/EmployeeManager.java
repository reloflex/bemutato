package employee;

import java.util.ArrayList;
import java.util.List;

import employee.type.EmployeeType;

public class EmployeeManager {
	List<Employee> employees = new ArrayList<>();
	
	public void loadEmployees() {
		// TODO: load from db
		Employee employeeEngineer = new Employee(EmployeeType.ENGINEER);
		employeeEngineer.setName("Beke Baba");
		
		Employee employeeSalesman = new Employee(EmployeeType.SALESMAN);
		employeeSalesman.setName("Teke Tata");
		
		Employee employeeManager = new Employee(EmployeeType.MANAGER);
		employeeManager.setName("Meke Mama");
		
		employees.add(employeeEngineer);
		employees.add(employeeSalesman);
		employees.add(employeeManager);
	}
	
	public void saveEmployees() {
		// TODO: store in db
	}
	
	public List<Employee> getEmployees() {
		return employees;
	}
	
	public void addEmployee(Employee employee) {
		employees.add(employee);
	}
	
	public void removeEmployee(Employee employee) {
		employees.remove(employee);
	}
}

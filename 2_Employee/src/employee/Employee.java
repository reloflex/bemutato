package employee;

import employee.level.EmployeeLevel;
import employee.type.EmployeeType;
import employee.salary.Salary;

/**
 * Example with Replace type code with state & Replace conditional with Polymorphism
 * @author Arnold
 *
 */
public class Employee {
	private String name;
	private EmployeeType type = EmployeeType.newType(EmployeeType.ENGINEER);
	private EmployeeLevel levelType = EmployeeLevel.JUNIOR;
	private int monthlySalary = Salary.BASE_MONTHLY_SALARY;
	private int commission = Salary.BASE_COMMISION;
	private int bonus = Salary.BASE_BONUS;

	
	public Employee (int type) {
		setType(type);
	}
	
	public Integer payAmount( ) {
		return type.payAmount(this);
	}
	
	public void upgradeEmployeeLevel() {
		if (getLevel() == EmployeeLevel.JUNIOR) {
			setLevel(EmployeeLevel.MID);
		} else if (getLevel() == EmployeeLevel.MID) {
			setLevel(EmployeeLevel.SENIOR);
		}
	}
	
	public void downgradeEmployeeLevel() {
		if (getLevel() == EmployeeLevel.SENIOR) {
			setLevel(EmployeeLevel.MID);
		} else if (getLevel() == EmployeeLevel.MID) {
			setLevel(EmployeeLevel.JUNIOR);
		}
	}
	
	public void upgradeEmployeeType() {
		if (type.getTypeCode() == EmployeeType.ENGINEER) {
			type = EmployeeType.newType(EmployeeType.SALESMAN);
		} else if (type.getTypeCode() == EmployeeType.SALESMAN) {
			type = EmployeeType.newType(EmployeeType.MANAGER);
		}
	}
	
	public void downgradeEmployeeType() {
		if (type.getTypeCode() == EmployeeType.MANAGER) {
			type = EmployeeType.newType(EmployeeType.SALESMAN);
		} else if (type.getTypeCode() == EmployeeType.SALESMAN) {
			type = EmployeeType.newType(EmployeeType.ENGINEER);
		}
	}

	public String getTypeName() {
		switch (getType()) {
			case EmployeeType.ENGINEER:
				return "Engineer";
			case EmployeeType.SALESMAN:
				return "Salesman";
			case EmployeeType.MANAGER:
				return "Manager";
			default:
				return "";
		}
	}
	
	public String getLevelName() {
		switch (getLevel()) {
			case JUNIOR:
				return "Junior";
			case MID:
				return "Middle";
			case SENIOR:
				return "Senior";
			default:
				return "";
		}
	}
	
	@Override
	public String toString() {
		return name + " - " + getTypeName();
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type.getTypeCode();
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = EmployeeType.newType(type);
	}
	
	/**
	 * @return the levelType
	 */
	public EmployeeLevel getLevel() {
		return levelType;
	}

	/**
	 * @param levelType the levelType to set
	 */
	public void setLevel(EmployeeLevel levelType) {
		this.levelType = levelType;
	}
	
	/**
	 * @return the monthlySalary
	 */
	public int getMonthlySalary() {
		return monthlySalary;
	}

	/**
	 * @param monthlySalary the monthlySalary to set
	 */
	public void setMonthlySalary(int monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	/**
	 * @return the commission
	 */
	public int getCommission() {
		return commission;
	}

	/**
	 * @param commission the commission to set
	 */
	public void setCommission(int commission) {
		this.commission = commission;
	}

	/**
	 * @return the bonus
	 */
	public int getBonus() {
		return bonus;
	}

	/**
	 * @param bonus the bonus to set
	 */
	public void setBonus(int bonus) {
		this.bonus = bonus;
	}

}

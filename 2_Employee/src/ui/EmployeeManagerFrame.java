package ui;

import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import employee.EmployeeManager;

public class EmployeeManagerFrame extends JFrame {

	private static final long serialVersionUID = 8382903019921471846L;
	
	private JPanel mainPanel;

	public EmployeeManagerFrame() { }
	
	public void init() {
		this.setTitle("Employee Manager");
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
		this.setBounds(100, 100, 1000, 720);
		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		
		EmployeeManager employeeManager = new EmployeeManager();
		employeeManager.loadEmployees();
		
		EmployeeListPanel listPanel = new EmployeeListPanel(employeeManager.getEmployees());
		listPanel.setOpaque(true);
        this.setContentPane(listPanel);

        this.setVisible(true);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
	}
	
}

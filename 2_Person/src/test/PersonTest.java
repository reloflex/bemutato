package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import model.Person;

class PersonTest {

	@Test
	void testMale() {
		Person female = Person.cerateFemale();
		assertEquals('F', female.getCode());
		assertEquals(false, female.isMale());
	}

	@Test
	void testFemale() {
		Person male = Person.cerateMale();
		assertEquals('M', male.getCode());
		assertEquals(true, male.isMale());
	}
	
}

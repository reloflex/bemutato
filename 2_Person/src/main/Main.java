package main;

import model.Person;

public class Main {
	public static void main(String[] args) {
		Person female = Person.cerateFemale();
		System.out.println("female code:" + female.getCode());
		
		Person male = Person.cerateMale();
		System.out.println("male code:" + male.getCode());
	}
}

package model;

public class Person {
	private final boolean isMale;
	private final char code;
	
	protected Person(boolean isMale, char code) {
		this.isMale = isMale;
		this.code = code;
	}
	
	public static Person cerateMale() {
		return new Person(true, 'M');
	}
	
	public static Person cerateFemale() {
		return new Person(false, 'F');
	}
	
	public boolean isMale() {
		return isMale;
	}

	public char getCode() {
		return code;
	}

}
